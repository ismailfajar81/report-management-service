package com.ismail.reportmanagement.controller;

import com.ismail.reportmanagement.entity.Report;
import com.ismail.reportmanagement.request.ReportRequest;
import com.ismail.reportmanagement.service.ReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/")
public class ReportController {

    private final ReportService reportService;

    @GetMapping("/")
    public ResponseEntity<List<Report>> getAllReport() {
        return ResponseEntity.ok(reportService.getReports());
    }

    @PostMapping("/report")
    public ResponseEntity<Report> createReport(@RequestBody ReportRequest request) {
        return ResponseEntity.ok(reportService.create(request));
    }

    @PatchMapping("/report/{id}")
    public ResponseEntity<Report> updateReport(@PathVariable("id") Long id, @RequestBody ReportRequest request) {
        return ResponseEntity.ok(reportService.update(id, request));
    }

    @DeleteMapping("/report/{id}")
    public ResponseEntity<Report> deleteReport(@PathVariable("id") Long id) {
        reportService.delete(id);
        return ResponseEntity.ok().build();
    }
}
