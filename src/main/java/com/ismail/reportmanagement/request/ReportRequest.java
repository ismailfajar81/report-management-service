package com.ismail.reportmanagement.request;

import com.ismail.reportmanagement.enumeration.EStatus;
import lombok.Data;

@Data
public class ReportRequest {

    private EStatus status;

}
