package com.ismail.reportmanagement.service;

import com.ismail.reportmanagement.entity.Report;
import com.ismail.reportmanagement.repository.ReportRepository;
import com.ismail.reportmanagement.request.ReportRequest;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ReportService {
    private final ReportRepository repository;

    public List<Report> getReports() {
        return repository.findAll();
    }

    public Report create(ReportRequest request) {
        Report report = new Report();
        BeanUtils.copyProperties(request, report);
        return repository.save(report);
    }

    public Report update(Long id, ReportRequest request) {
        Optional<Report> optionalReport = repository.findById(id);
        if (!optionalReport.isPresent()) {
            throw new EntityNotFoundException("Report not present in the database");
        }
        Report report = optionalReport.get();
        report.setStatus(request.getStatus());
        return repository.save(report);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
}
