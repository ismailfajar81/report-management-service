package com.ismail.reportmanagement.entity;

import com.ismail.reportmanagement.enumeration.EStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "report")
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "no_resi", length = 50, unique = true)
    private Long noResi;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", columnDefinition = "ENUM('APPROVED', 'REJECTED')", nullable = false)
    private EStatus status;

    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDateTime createdAt;
}
