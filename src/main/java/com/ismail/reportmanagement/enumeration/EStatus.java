package com.ismail.reportmanagement.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum EStatus {
    APPROVED,
    REJECTED;

    @JsonCreator
    public static EStatus create(String value) {
        return EStatus.valueOf(value.toUpperCase());
    }
}
